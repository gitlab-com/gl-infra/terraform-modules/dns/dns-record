variable "records" {
  type = map(object({
    records = list(object({
      prio  = number
      value = string
    }))
    ttl = number
  }))
  description = "MX record values."
}

variable "zone" {
  type        = string
  description = "Zone. Fully qualified, with trailing period. E.g. `gitlab.com.`."

  validation {
    condition     = can(regex("[\\w-]+\\.[\\w-]+\\.$", var.zone))
    error_message = "The zone must be a valid FQDN with a trailing period."
  }
}
