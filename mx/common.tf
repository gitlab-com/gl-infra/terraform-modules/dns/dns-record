/**
* This file contains magic and a copy should be present in each submodule of the DNS module
*/

locals {
  zones = jsondecode(file("${path.module}/../zones.json"))
}
