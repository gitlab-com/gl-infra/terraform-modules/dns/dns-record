# MX DNS Record

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 4.0 |
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | >= 4.39.0, < 5 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 4.0 |
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | >= 4.39.0, < 5 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_route53_record.mx](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [cloudflare_record.mx](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_records"></a> [records](#input\_records) | MX record values. | <pre>map(object({<br/>    records = list(object({<br/>      prio  = number<br/>      value = string<br/>    }))<br/>    ttl = number<br/>  }))</pre> | n/a | yes |
| <a name="input_zone"></a> [zone](#input\_zone) | Zone. Fully qualified, with trailing period. E.g. `gitlab.com.`. | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->