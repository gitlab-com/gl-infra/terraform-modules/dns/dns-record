variable "records" {
  type = map(object({
    records = list(string)
    ttl     = number
    proxied = optional(bool, false)
    spectrum_config = optional(object({
      ip_firewall           = optional(bool, true)
      ip_override           = optional(list(string), [])
      ports                 = map(string)
      proxy_protocol        = optional(string, "off")
      edge_ips_type         = optional(string, "dynamic")
      edge_ips_connectivity = optional(string, "all")
      argo_smart_routing    = optional(bool, false)
    }))
  }))
  description = "A/AAAA record values and optional Cloudflare configuration."
}

variable "zone" {
  type        = string
  description = "Zone. Fully qualified, with trailing period. E.g. `gitlab.com.`."

  validation {
    condition     = can(regex("[\\w-]+\\.[\\w-]+\\.$", var.zone))
    error_message = "The zone must be a valid FQDN with a trailing period."
  }
}

variable "type" {
  type        = string
  description = "A or AAAA."

  validation {
    condition     = contains(["A", "AAAA"], var.type)
    error_message = "The type must be A or AAAA."
  }
}
