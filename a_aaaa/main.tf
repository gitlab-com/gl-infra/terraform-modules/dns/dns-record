resource "aws_route53_record" "a_aaaa" {
  /**
   * The type of the record is prefixed, to prevent naming collisions.
   * The record will only be created if the zone was configured for route53.
   */
  for_each = {
    for k, v in var.records : "${var.type}_${k}" => merge({ name = k }, v)
    if contains(keys(local.zones.route53), var.zone)
  }

  zone_id = local.zones.route53[var.zone]
  name    = each.value.name
  type    = var.type
  ttl     = each.value.ttl
  records = each.value.records
}

resource "cloudflare_record" "a_aaaa" {
  /**
   * If there is no spectrum_config, this is just a regular record,
   * it will be stretched out for each value in the records array to please CF.
   * Additionally the type of the record is prefixed, to prevent naming collisions.
   * The record will only be created if the zone was configured for cloudflare.
   */
  for_each = {
    for flatv in flatten([
      for k, v in var.records : [
        for idx, record in v.records : {
          key     = "${var.type}_${k}_${idx}"
          name    = k
          ttl     = v.ttl
          value   = record
          proxied = v.proxied
        } if v.spectrum_config == null
      ] if contains(keys(local.zones.cloudflare), var.zone)
    ]) : flatv.key => flatv
  }

  zone_id = local.zones.cloudflare[var.zone]
  name    = each.value.name
  type    = var.type
  ttl     = each.value.proxied ? 1 : max(30, each.value.ttl)
  content = each.value.value
  proxied = each.value.proxied
}

resource "cloudflare_spectrum_application" "a_aaaa" {
  /**
   * This is a tad more complicated.
   * If we have spectrum_config.ip_override configured, those IPs will be used.
   * The spectrum app supports IPv6 and IPv4 origins. This is differentiated by A and AAAA record types.
   * An A/AAAA record becomes a spec app, as soon as it has spectrum_config defined.
   * a `spectrum_config.proxy_protocol` key is required. It's values may be, `v1`, `v2`, `simple` or `off`. It is ignored for non direct traffic ports
   * a map of `port` => `traffic type` is required as key `spectrum_config.ports`. The traffic type can be either `http`, `https` or `direct`. Alternatively it can be a different target port. The mode will then be forced to `direct`.
   * The record will only be created if the zone was configured for cloudflare.
   */
  for_each = {
    for flatv in flatten([
      for k, v in var.records : [
        for cf_port, target in v.spectrum_config.ports : {
          key             = "${k}_tcp/${cf_port}"
          name            = k
          ttl             = v.ttl
          spectrum_config = v.spectrum_config
          ips             = coalescelist(v.spectrum_config.ip_override, v.records)
          cf_port         = cf_port
          origin_port     = length(regexall("^[0-9]+$", target)) > 0 ? target : cf_port
          mode            = length(regexall("^[0-9]+$", target)) > 0 ? "direct" : target
        }
      ] if contains(keys(local.zones.cloudflare), var.zone) && v.spectrum_config != null
    ]) : flatv.key => flatv
  }

  zone_id      = local.zones.cloudflare[var.zone]
  protocol     = "tcp/${each.value.cf_port}"
  traffic_type = each.value.mode

  edge_ips {
    type         = each.value.spectrum_config.edge_ips_type
    connectivity = each.value.spectrum_config.edge_ips_connectivity
  }

  # If we are using direct mode, we may enable the ip_firewall (default: true), otherwhise, force to false.
  ip_firewall = each.value.mode == "direct" && each.value.spectrum_config.ip_firewall

  # We need to create the URL list differently for IPv4 and IPv6.
  origin_direct = var.type == "AAAA" ? [
    for ip in each.value.ips : "tcp://[${ip}]:${each.value.origin_port}"
    ] : [
    for ip in each.value.ips : "tcp://${ip}:${each.value.origin_port}"
  ]

  # We may only specify proxy_protocol if we are in direct mode
  proxy_protocol = each.value.mode == "direct" ? each.value.spectrum_config.proxy_protocol : null

  argo_smart_routing = each.value.spectrum_config.argo_smart_routing

  dns {
    type = "CNAME"
    name = trimsuffix(each.value.name, ".")
  }
}
