# A/AAAA DNS Record

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 4.0 |
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | >= 4.39.0, < 5 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 4.0 |
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | >= 4.39.0, < 5 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_route53_record.a_aaaa](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [cloudflare_record.a_aaaa](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [cloudflare_spectrum_application.a_aaaa](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/spectrum_application) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_records"></a> [records](#input\_records) | A/AAAA record values and optional Cloudflare configuration. | <pre>map(object({<br/>    records = list(string)<br/>    ttl     = number<br/>    proxied = optional(bool, false)<br/>    spectrum_config = optional(object({<br/>      ip_firewall           = optional(bool, true)<br/>      ip_override           = optional(list(string), [])<br/>      ports                 = map(string)<br/>      proxy_protocol        = optional(string, "off")<br/>      edge_ips_type         = optional(string, "dynamic")<br/>      edge_ips_connectivity = optional(string, "all")<br/>      argo_smart_routing    = optional(bool, false)<br/>    }))<br/>  }))</pre> | n/a | yes |
| <a name="input_type"></a> [type](#input\_type) | A or AAAA. | `string` | n/a | yes |
| <a name="input_zone"></a> [zone](#input\_zone) | Zone. Fully qualified, with trailing period. E.g. `gitlab.com.`. | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->