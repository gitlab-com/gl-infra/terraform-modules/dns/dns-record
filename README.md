# GitLab.com DNS Record Terraform Module

## What is this?

This module is the GitLab way to DNS. It handles all zone information internally and provides a unified interface for handling DNS records across providers.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

## Noteworthy details

In an effort to not repeat something like [this](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/8794), this modules is to be regarded as the single point of which provider each zone is deployed to. This configuration is held in `zones.json` and also controls dual deployments to multiple providers.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.4 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_a"></a> [a](#module\_a) | .//a_aaaa | n/a |
| <a name="module_aaaa"></a> [aaaa](#module\_aaaa) | .//a_aaaa | n/a |
| <a name="module_caa"></a> [caa](#module\_caa) | .//caa | n/a |
| <a name="module_cname"></a> [cname](#module\_cname) | .//generic | n/a |
| <a name="module_mx"></a> [mx](#module\_mx) | .//mx | n/a |
| <a name="module_ns"></a> [ns](#module\_ns) | .//generic | n/a |
| <a name="module_soa"></a> [soa](#module\_soa) | .//generic | n/a |
| <a name="module_txt"></a> [txt](#module\_txt) | .//generic | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_a"></a> [a](#input\_a) | A records. | <pre>map(object({<br/>    records = list(string)<br/>    ttl     = optional(number, 1)<br/>    proxied = optional(bool, false)<br/>    spectrum_config = optional(object({<br/>      ip_firewall           = optional(bool, true)<br/>      ip_override           = optional(list(string), [])<br/>      ports                 = map(string)<br/>      proxy_protocol        = optional(string, "off")<br/>      edge_ips_type         = optional(string, "dynamic")<br/>      edge_ips_connectivity = optional(string, "all")<br/>      argo_smart_routing    = optional(bool, false)<br/>    }))<br/>  }))</pre> | `{}` | no |
| <a name="input_aaaa"></a> [aaaa](#input\_aaaa) | AAAA records. | <pre>map(object({<br/>    records = list(string)<br/>    ttl     = optional(number, 1)<br/>    proxied = optional(bool, false)<br/>    spectrum_config = optional(object({<br/>      ip_firewall           = optional(bool, true)<br/>      ip_override           = optional(list(string), [])<br/>      ports                 = map(string)<br/>      proxy_protocol        = optional(string, "off")<br/>      edge_ips_type         = optional(string, "dynamic")<br/>      edge_ips_connectivity = optional(string, "all")<br/>      argo_smart_routing    = optional(bool, false)<br/>    }))<br/>  }))</pre> | `{}` | no |
| <a name="input_caa"></a> [caa](#input\_caa) | CAA records. | <pre>map(object({<br/>    records = list(object({<br/>      flags = number<br/>      tag   = string<br/>      value = string<br/>    }))<br/>    ttl = number<br/>  }))</pre> | `{}` | no |
| <a name="input_cname"></a> [cname](#input\_cname) | CNAME records. | <pre>map(object({<br/>    records = list(string)<br/>    proxied = optional(bool, false)<br/>    ttl     = optional(number, 1)<br/>  }))</pre> | `{}` | no |
| <a name="input_mx"></a> [mx](#input\_mx) | MX records. | <pre>map(object({<br/>    records = list(object({<br/>      prio  = number<br/>      value = string<br/>    }))<br/>    ttl = number<br/>  }))</pre> | `{}` | no |
| <a name="input_ns"></a> [ns](#input\_ns) | NS records. | <pre>map(object({<br/>    records = list(string)<br/>    proxied = optional(bool, false)<br/>    ttl     = number<br/>  }))</pre> | `{}` | no |
| <a name="input_soa"></a> [soa](#input\_soa) | SOA records. | <pre>map(object({<br/>    records = list(string)<br/>    proxied = optional(bool, false)<br/>    ttl     = number<br/>  }))</pre> | `{}` | no |
| <a name="input_txt"></a> [txt](#input\_txt) | TXT records. | <pre>map(object({<br/>    records = list(string)<br/>    proxied = optional(bool, false)<br/>    ttl     = optional(number, 1)<br/>  }))</pre> | `{}` | no |
| <a name="input_zone"></a> [zone](#input\_zone) | Zone. Fully qualified, with trailing period. E.g. `gitlab.com.`. | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->

### Zone configuration

Zones to be used with this module are configured in the `zones.json`.

Each provider has a key. Currently implemented are AWS Route53 (`route53`) and Cloudflare (`cloudflare`).
Each zone to be used with a given provider has to be specified with a trailing period (`.`). The value must be the `zone_id` of that zone with the provider of the block.
A zone might exist in multiple providers. In that case a record will be deployed to all providers where that zone is configured.

Example `zones.json`:
```
{
  "route53": {
    "gitlab.net.": "Z3NS4ELSKYGTY9",
    "gitlab.com.": "Z31LJ6JZ6X5VSQ",
    "gitlab.io.": "Z1UCC9JLXES5NS"
  },
  "cloudflare": {
    "gitlab.net.": "736d3823d5085e26501019cbc313b20e",
    "gitlab.com.": "5b1bc06af128a829167e3a1212d86c28",
    "staging.gitlab.com.": "7e9c74d7c62f2f734c1d1b048b0844be"
  }
}
```
This would lead to `gitlab.net.` and `gitlab.com.` to be deployed to both Route53 as well as Cloudflare, whereas `staging.gitlab.com.` and `gitlab.io.` are exclusively deployed to each Cloudflare and Route53 respectively.

### Record configuration

Any of the variables above are configured via the same underlying format. Recordtypes can be mixed within the same module definition.

The key of the map should be the FQDN of the record you want to create, with a trailing period (`.`). E.g. `about.gitlab.com.`. The value of that key is another map, with an array `records` containing the actual records and a `ttl` field, with the TTL for the record.

For every record, except MX and CAA records the record values are strings.
For MX records it is a map of `prio` and `value`. An MX record `1 ASPMX.L.GOOGLE.COM.` would become `{prio: 1, value: "ASPMX.L.GOOGLE.COM."}`.
For CAA records it is a map of `flags`, `tag` and `value`. For example `0 issue "digicert.com"` would become `{flags: 0, tag: "issue", value: "digicert.com"}`.

Cloudflare Spectrum Applications are configured via A and AAAA records via an additional `spectrum_config` field within the individual record.
If another DNS provider is configured for the Zone of the record, a regular A/AAAA record will be created with that provider.
If the zone is enabled for Cloudflare and this field is configured, there will not be an A/AAAA record created with Cloudflare, but a Spectrum Application instead.

If we have `spectrum_config.ip_override` configured, those IPs will be used instead of the IPs of the record.
The spectrum app supports IPv6 and IPv4 origins. This is differentiated by A and AAAA record types. The exposed endpoint will always support both IPv6 and IPv4, regardless of the origin's support.
A `spectrum_config.proxy_protocol` key is required. It's values may be, `v1`, `v2`, `simple` or `off`. It is ignored for non direct traffic ports.
A map of `port` => `traffic type` is required as the value for `spectrum_config.ports`. The traffic type can be either `http`, `https` or `direct`. Alternatively it can be a different target port. The mode will then be forced to `direct`.

An example of a Cloudflare Spectrum Application:
```
module "spectrum_example" {
  source = "ops.gitlab.net/gitlab-com/dns-record/dns"
  version = "X.Y.Z"

  zone   = "gitlab.com."
  a = {
    "gitlab.com." = {
      records = ["35.231.145.151"]
      spectrum_config = {
        proxy_protocol = "v2"
        ports = {
          "22"  = "direct",
          "80"  = "http",
          "443" = "https",
          "1234" = "12345",
        }
        ip_override = [
          "1.2.3.4",
        ]
      }
      ttl = 300
    }
  }
}
```
With a Route53 configured zone, this would create an A record pointing to `35.231.145.151`.
With a Cloudflare configured zone, this would create a Spectrum app pointing to `1.2.3.4` with ports `22` (proxied with PROXY protocol v2), `80` (via the HTTP pipeline) and `443` (via the HTTPS pipeline). Port `1234` exposed on Cloudflare will reach port `12345` on the target (proxied with PROXY protocol v2, because of the `proxy_protocol` value).

An example of a regular A record:
```
module "a_example" {
  source = "ops.gitlab.net/gitlab-com/dns-record/dns"
  version = "X.Y.Z"

  zone   = "gitlab.com."
  a = {
    "about.gitlab.com." = {
      records = [
        "1.2.3.4",
        "5.6.7.8",
        "9.0.1.2",
      ]
      ttl = 300
    }
  }
}
```
For Route53 and Cloudflare configured zones the result would be an A record pointing to those IPs.

Examples for CAA, TXT and MX records and combined module usage:
```
module "combined_example" {
  source = "ops.gitlab.net/gitlab-com/dns-record/dns"
  version = "X.Y.Z"

  zone   = "gitlab.com."
  caa = {
    "gitlab.com." = {
      records = [
        { flags = 0, tag = "issue", value = "digicert.com" },
        { flags = 0, tag = "iodef", value = "mailto:security@gitlab.com" },
      ]
      ttl = 300
    }
  }
  txt = {
    "gitlab.com." = {
      records = [
        "this is a TXT record",
        "this is one, too",
      ]
      ttl = 300
    }
  }
  mx = {
    "gitlab.com." = {
      records = [
        { prio = 1, value = "ASPMX.L.GOOGLE.COM." },
        { prio = 5, value = "ALT1.ASPMX.L.GOOGLE.COM." },
      ]
      ttl = 300
    }
  }
}
```

## Prerequisites

The Cloudflare and AWS provider have to be configured in your code outside of this module.

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
