resource "aws_route53_record" "caa" {
  /**
   * We need to format the records into a string, that is what we do here.
   * The record will only be created if the zone was configured for route53.
   */
  for_each = {
    for k, v in var.records : k => {
      ttl = v.ttl
      records = [
        for record in v.records : "${record.flags} ${record.tag} \"${record.value}\""
      ]
    } if contains(keys(local.zones.route53), var.zone)
  }

  zone_id = local.zones.route53[var.zone]
  name    = each.key
  type    = "CAA"
  ttl     = each.value.ttl
  records = each.value.records
}

resource "cloudflare_record" "caa" {
  /**
   * Extract values array into separate records, flatten those and create a map of them to iterate over.
   * The record will only be created if the zone was configured for cloudflare.
   */
  for_each = {
    for flatv in flatten([
      for k, v in var.records : [
        for idx, record in v.records : {
          key  = "${k}_${idx}"
          name = k
          ttl  = v.ttl
          data = record
        }
      ] if contains(keys(local.zones.cloudflare), var.zone)
    ]) : flatv.key => flatv
  }

  zone_id = local.zones.cloudflare[var.zone]
  name    = each.value.name
  type    = "CAA"
  ttl     = max(30, each.value.ttl)

  data {
    flags = each.value.data.flags
    tag   = each.value.data.tag
    value = each.value.data.value
  }
}
