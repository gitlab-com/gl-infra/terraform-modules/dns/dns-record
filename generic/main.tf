resource "aws_route53_record" "generic" {
  /**
   * The type of the record is prefixed, to prevent naming collisions.
   * The record will only be created if the zone was configured for route53.
   */
  for_each = {
    for k, v in var.records : "${var.type}_${k}" => merge({ name = k }, v)
    if contains(keys(local.zones.route53), var.zone)
  }

  zone_id = local.zones.route53[var.zone]
  name    = each.value.name
  type    = var.type
  ttl     = each.value.ttl
  records = each.value.records
}

resource "cloudflare_record" "generic" {
  /**
   * Extract values array into separate records, flatten those and create a map of them to iterate over.
   * Ignoring SOA and NS records as those are not supported nor needed by Cloudflare.
   * Additionally the type of the record is prefixed, to prevent naming collisions.
   * The record will only be created if the zone was configured for cloudflare.
   */
  for_each = contains(["SOA"], var.type) ? {} : {
    for flatv in flatten([
      for k, v in var.records : [
        for idx, record in v.records : {
          key     = "${var.type}_${k}_${idx}"
          name    = k
          ttl     = v.ttl
          value   = record
          proxied = v.proxied
        }
      ] if contains(keys(local.zones.cloudflare), var.zone)
    ]) : flatv.key => flatv if !(var.type == "NS" && flatv.name == var.zone)
  }

  zone_id = local.zones.cloudflare[var.zone]
  name    = each.value.name
  type    = var.type
  ttl     = each.value.proxied ? 1 : max(30, each.value.ttl)
  // For some record types we need to chomp off the trailing period.
  content = contains(["CNAME", "NS"], var.type) ? trimsuffix(lower(each.value.value), ".") : each.value.value
  proxied = each.value.proxied
}
