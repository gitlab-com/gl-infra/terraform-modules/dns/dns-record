variable "records" {
  type = map(object({
    records = list(string)
    proxied = optional(bool, false)
    ttl     = number
  }))
  description = "DNS record values."
}

variable "zone" {
  type        = string
  description = "Zone. Fully qualified, with trailing period. E.g. `gitlab.com.`."

  validation {
    condition     = can(regex("[\\w-]+\\.[\\w-]+\\.$", var.zone))
    error_message = "The zone must be a valid FQDN with a trailing period."
  }
}
variable "type" {
  type        = string
  description = "Any DNS record type, that does not need any special treatment."

  validation {
    condition     = contains(["CNAME", "NS", "SOA", "TXT"], var.type)
    error_message = "The type must be CNAME, NS, SOA or TXT."
  }
}
